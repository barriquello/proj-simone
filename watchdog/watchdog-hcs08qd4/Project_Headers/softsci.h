/*******************************************************************************
*
* Motorola Inc.
* (c) Copyright 2003 Motorola, Inc.
* ALL RIGHTS RESERVED.
*
********************************************************************************
*
* $File Name: pcmastersoftsci.h$
*
* Description: Software SCI headers for 
*              PC Master Communication protocol
*
* $Version: 1.1.1.0$
* $Date: Nov-27-2003$
* $Last Modified By: r30323$
*
*******************************************************************************/
#include "derivative.h"

/* Software SCI API */
extern char SCI0Read(void);
extern void SCI0Write(char ch);
extern void SCI0RxEnable(void);
extern void SCI0InterruptTx_CB (void);
extern void SCI0InterruptRx_CB (void);
extern void	SCI0Init(void);
/* Software SCI API end */

#pragma DATA_SEG SHORT _DATA_ZEROPAGE
/*------SCI definitions registers-------------------*/
extern char SCDR; /*SCI data register*/
#pragma DATA_SEG DEFAULT

#define BUS_CLOCK_HZ 4000000 /* reqd' bus clock in Hz */

/*##################################*/
/*##################################*/

/*##################################*/
#define BAUDRATE 9600L

#define TMRMODULO 0x4ffe        // specify the modulo (mask) in which 'free' running timer operates
/*##################################*/

/*##################################*/
/*### common softSCI section */
//#define SCISINGLEWIRE           // define only if RXD & TXD pins are shared (ie. single wire)
//#define SCIINV                   // define this one, if SCI needs to be inverted (ie. non-standard interface)
/*##################################*/

/*##################################*/
/*### TXD pin section */
//#define SCITXDPINISTIMERPIN     // defined if TXD pin can use hw output compare feature
/*##################################*/

#ifndef SCITXDPINISTIMERPIN
#define TXDPIN      PTAD_PTAD0
#define TXDPINDDR   PTADD_PTADD0

#define TXDPINPUE   PTAPE_PTAPE0
#endif

/*##################################*/
/*### RXD pin section */
#define SCIRXDPINISTIMERPIN     // defined if RXD pin can use hw input capture feature
#define RXDPIN      PTAD_PTAD0
#define RXDPINDDR   PTADD_PTADD0
#define RXDPINPORT  PTAD
#define RXDPINMASK  0x01
/*##################################*/

#ifndef SCIRXDPINISTIMERPIN  	// if RXD is not timer pin, it must be KBI pin
    #ifdef SCIINV
        #error "Cannot use SCIINV and !SCIRXDPINISTIMERPIN features together!"
    #endif
    #define KBIECH KBIPE_KBIPE0  // and you must define your KBIE here
#endif

/* softSCI timer selection section */        
/* must be one of timer channels, if SCIRXDPINISTIMERPIN and/or SCITXDPINISTIMERPIN
   macros are defined, it must also match the appropriate hardware (pin) */
#define SCITSC      TPMSC
#define SCITSCCH    TPMC0SC
#define SCITSC_CHF  TPMC0SC_CH0F
#define SCITSC_IE   TPMC0SC_CH0IE
#define SCITCNT     TPMCNT
#define SCITCH      TPMC0V
#define SCITMOD     TPMMOD
#define IV_SCITMR   VectorNumber_Vtpm1ch0
/* end */

#define DDRIN  0
#define DDROUT 1

#ifndef SCIINV
    #define TXDPINSET() TXDPIN=1
    #define TXDPINCLR() TXDPIN=0
#else
    #define TXDPINSET() TXDPIN=0
    #define TXDPINCLR() TXDPIN=1
#endif

#define SCITX 1
#define SCIRX 2
