#include <hidef.h> /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */

/* port A pin configuration */
#define PORT					PTAD
#define PORT_DIR				PTADD
#define PORT_DS					PTADS
#define PIN_MCU_UART_INPUT		0 	/* input to receive a UART command to reset modem - AT+RST */
#define PIN_MODEM_ALIVE			1	/* used to check if modem is alive - modem TX */
#define PIN_MODEM_FORCE_RESET	2	/* input switch used to force a modem reset */
#define PIN_MODEM_RESET			3	/* used to reset modem */

/* app configuration */
#define MAX_TIMEOUT_TO_RESET_SEC	300   /* 300s =  5min */
#define PERIODIC_REBOOT_TIMEOUT		21600	/* 21600s = 6hrs */
#define MAX_CMD_SIZE   				15

#define IN  	0
#define OUT 	1

/* globals */
volatile unsigned char do_reset = 0;
volatile unsigned int timer = 0;
volatile unsigned int periodic_reboot = 0;
volatile unsigned int max_timeout = MAX_TIMEOUT_TO_RESET_SEC;

static char cmd_rx[MAX_CMD_SIZE+1];
static char cmd_idx = 0;


#include "softsci.h"

unsigned char bitset(unsigned char var, unsigned char pin, unsigned char val)
{
	unsigned char mask = 1 << pin;
	unsigned char value = val << pin;

	return (var & ~mask) | (value & mask);
}

unsigned char bitget(unsigned char var, unsigned char pin)
{
	unsigned char mask = (1 << pin);
	return (var & mask) >> (pin & 0x07);
}

void gpio_set_dir(unsigned char pin, unsigned char dir)
{
	PORT_DIR = bitset(PORT_DIR,pin,dir);
}

void gpio_write(unsigned char pin, unsigned char val)
{
	PORT = bitset(PORT,pin,val);
}

unsigned char gpio_read(unsigned char pin)
{
	return (bitget(PORT,pin)==0);
}

void test_bitsetget(void)
{
	unsigned char x = 0;
	x=bitset(x,3, 1);
	if(bitget(x,3) != 1)
	{
	  x=0;
	}
	x=0xFF;
	x=bitset(x,3, 0);
	if(bitget(x,3) != 0)
	{
	  while(1);
	}
}

const char *s = "AT+RST\r\n";

void test_cmd_rx_modem_reset(void)
{

	char *r = (char*)s;
	while(*r)
	{
		SCDR = *r++;
		SCI0InterruptRx_CB();
	}
	if(do_reset == 1)
	{
		do_reset = 0;
	}
}

void delay_ms(unsigned int t)
{
	volatile unsigned int ms_1 = 0;
	while(t-- > 0)
	{
		ms_1 = 140;
		__RESET_WATCHDOG();
		while(ms_1-- >0);
	}
}

void modem_on_off(void)
{
	gpio_write(PIN_MODEM_RESET, 1);
	delay_ms(2000);
	gpio_write(PIN_MODEM_RESET, 0);
}

void modem_reset(void)
{
	modem_on_off();
	delay_ms(15000);
	modem_on_off();
}

void TimerInit(void);
void ModemAliveISRInit(void);

/* projeto de sw */
/*
 se pino de reset == 1 ou
 se recebeu comando_reset == 1 ou
 se timer > max_timer
   => executa reset
--------------------------
  a cada isr timer timer++
  a cada isr modem tx - timer = 0
  a cada isr uart rx
  	 - armazena caractere
  	 - se caractere == \n
  	 - se comando ==  at+rst
  	 - flag comando_reset == 1
  	 - se comando ==  at+mxt= max_timer_val
  	 	 max_timer = max_timer_val
 */

void main(void)
{

  test_bitsetget();
  test_cmd_rx_modem_reset();
  delay_ms(10);

  /* Init ports */
  PORT_DS = bitset(PORT_DS, PIN_MODEM_RESET, 1); /* increase drive strength for output */
  gpio_write(PIN_MODEM_RESET, 0);
  gpio_set_dir(PIN_MODEM_RESET, OUT);

  gpio_set_dir(PIN_MODEM_ALIVE, IN);
  gpio_set_dir(PIN_MODEM_FORCE_RESET, IN);

  gpio_write(PIN_MODEM_RESET, 1); /* Initiate the module, when powered on */
  delay_ms(5000);
  gpio_write(PIN_MODEM_RESET, 0);

  SCI0Init();
  TimerInit(); /* configure timer for 1sec ISR */
  ModemAliveISRInit();

  SCI0Write('a');
  EnableInterrupts;

  do_reset = 0;

  for(;;)
  {

	  if(gpio_read(PIN_MODEM_FORCE_RESET) ||
		 (do_reset == 1))
	  {
		  do_reset = 0;
		  modem_reset();
		  delay_ms(2000);
		  timer = 0;
			periodic_reboot = 0;
	  }

      __RESET_WATCHDOG();	/* feeds the dog */
  } /* loop forever */
  /* please make sure that you never leave main */
}


void TimerInit(void)
{
	#define PRESCALA 	7
	TPM2SC = 0;
	TPM2CNT = 0;
	TPM2MOD = BUS_CLOCK_HZ/(1<<PRESCALA);
	TPM2SC = 0x48 | PRESCALA; /* enable overflow isr */
}

void ModemAliveISRInit(void)
{
	KBISC_KBIE = 0;
	KBIPE = bitset(KBIPE, PIN_MODEM_ALIVE, 1);
	KBISC_KBACK = 1;
	KBISC_KBIE = 1; /* enable isr */
}

static int strcmp(char *s1, char *s2)
{
	for (; *s1 == *s2; s1++, s2++)
	{
		if (*s1 == '\0')
		{
			return 0;
		}
	}
	return *s1 - *s2;
}

static void memclr(char *s1, int len)
{
	while (len>0)
	{
		len--;
		*s1++ = '\0';
	}
}

static int strlen(char *s)
{
	int c = 0;
	while(*s++ != '\0') c++;
	return c;
}

#define CMD_RESET "AT+RST"

void SCI0InterruptRx_CB(void)
{
	char c = SCI0Read();
	cmd_rx[cmd_idx++] = c;
	if(cmd_idx == MAX_CMD_SIZE || c == '\n')
	{
		cmd_rx[MAX_CMD_SIZE] = '\0';
		cmd_idx = 0;
		/* process cmd rx */
		if((char)strcmp(cmd_rx, CMD_RESET) >= (char)strlen(CMD_RESET))
		{
			memclr(cmd_rx, sizeof(cmd_rx));
			do_reset = 1;
		}

	}

}

/* interrupts */
__interrupt VectorNumber_Vtpm2ovf void timer_isr(void)
{
	TPM2SC_TOF = 0; // Limpa a flag de condição de estouro do temporizador
	if(++timer >= max_timeout)
	{
		timer = 0;
		do_reset = 1;
	}
	if (++periodic_reboot >= PERIODIC_REBOOT_TIMEOUT){
		periodic_reboot = 0;
		do_reset = 1;
	}
}

__interrupt VectorNumber_Vkeyboard1 void modem_tx_isr(void)
{
    KBISC_KBACK = 1;         // confirm interrupt
    timer = 0;				 // reset timer counter
}
