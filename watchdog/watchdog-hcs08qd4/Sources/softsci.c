/*******************************************************************************
*
* Motorola Inc.
* (c) Copyright 2003 Motorola, Inc.
* ALL RIGHTS RESERVED.
*
********************************************************************************
*
* $File Name: pcmastersoftsci.c$
*
* Description: Software SCI library for 
*              PC Master Communication protocol
*
* $Version: 1.1.1.0$
* $Date: Nov-27-2003$
* $Last Modified By: r30323$
*
*******************************************************************************/
#include "derivative.h"
#include "softsci.h"

#pragma DATA_SEG SHORT _DATA_ZEROPAGE
/*------SCI definitions registers-------------------*/
char SCDR; /*SCI data register*/

unsigned char SciBuff;
unsigned char SciPort;
unsigned char SciCnt;
unsigned char SciStat;
unsigned int SciTmr, BAUDTICK;
#pragma DATA_SEG DEFAULT


#define SCI0InterruptTx_CB()

void SCI0Init(void)
{

	BAUDTICK = BUS_CLOCK_HZ / BAUDRATE;

    SCITMOD = TMRMODULO;
    SCITSC = 0;            // run timer, no prescaling, no modulo int.
    SCI0RxEnable();

#ifndef SCITXDPINISTIMERPIN
    TXDPINPUE = 1;          // enable pull-up
#endif
};

char SCI0Read(void)
{
    return SCDR;
}

void SCI0Write(char ch)
{

#ifndef SCIRXDPINISTIMERPIN
    KBIECH = 0;             // disable RX KBI int'
#endif
  #ifndef SCIINV
    SCITSCCH = 0x00;        // reset timer logic so no false edge appears
  #else
    SCITSCCH = 0x10;        // reset timer logic so no false edge appears
  #endif
#ifndef SCITXDPINISTIMERPIN
    TXDPINSET();            // just make sure no glitch (high to low) appears
    TXDPINDDR = DDROUT;     // TXD pin output
#endif
    SciStat = SCITX;
    SciBuff = ch;           // copydown the timer
    SciCnt = 9;             // 8 bits of data + stop bits to send
    
#ifdef SCITXDPINISTIMERPIN
  #ifndef SCIINV
    SCITSCCH = 0x18;        // output compare, falling edge
  #else
    SCITSCCH = 0x1C;        // output compare, rising edge
  #endif
    SCITCH = SciTmr = ((SciTmr = SCITCNT) + BAUDTICK) & TMRMODULO;
#else
    SCITSCCH = 0x10;        // just timer int to be scheduled (just port control)
    SCITCH = SciTmr = ((SciTmr = SCITCNT) + BAUDTICK) & TMRMODULO;
    TXDPINCLR();            // TXD pin low (start bit)
#endif

    SCITSC_CHF = 0;	        // clearing timer flag
    SCITSC_IE = 1;          // enable tmr. channel interrupts
}

void SCI0RxEnable(void)
{
    SCITSC_IE = 0;          // disable tmr. channel interrupts

    RXDPINDDR = DDRIN;      // RXD pin input

    SciStat = SCIRX;
    SciCnt = 0;             // sci cnt will be falling edge
    
#ifdef SCIRXDPINISTIMERPIN
  #ifndef SCIINV
    SCITSCCH = 0x08;        // input capture, falling edge only on tmr.
  #else
    SCITSCCH = 0x04;        // input capture, rising edge only on tmr.
  #endif
    SCITSC_CHF = 0;	        // clearing timer flag
    SCITSC_IE = 1;          // enable tmr. channel interrupts
#else
    /* specify RXD fallling edge interrupt init here! */
    KBSCR_IMASKK = 1;       // mask int now (safe int init)
    KBSCR_MODEK = 0;        // edge only
    KBIECH = 1;             // enable pin specific KBI int'
    KBSCR_ACKK = 1;         // confirm interrupt
    KBSCR_IMASKK = 0;       // unmask int now
#endif
}

#ifndef SCIRXDPINISTIMERPIN
__interrupt IV_KBRD void Kbd_int(void)
{
    SCITCH = SciTmr = ((SciTmr = SCITCNT) + BAUDTICK/2) & TMRMODULO;

    SciCnt++;
    SCITSC_CHF = 0;	        // clearing timer flag
    SCITSCCH = 0x50;        // timer int to be scheduled (keep int enabled)

    KBIECH = 0;             // and disable KBI int - all subsequent ints are timer driven
    KBSCR_ACKK = 1;         // confirm interrupt
}
#endif

__interrupt IV_SCITMR void Timer_int(void)
{
    SciPort = RXDPINPORT;       // as fast as possible port scan for receive branch

    if (SciStat == SCITX)
    {

#ifdef SCITXDPINISTIMERPIN
        if (SciCnt > 1)
        {
            SCITCH = SciTmr = (SciTmr + BAUDTICK) & TMRMODULO;
            SciCnt--;                   // decrement counter
    #ifndef SCIINV
            SCITSCCH = 0x58 | (SciBuff & 0x01?0x04:0);        // output compare, schedule clear output (start bit)            
    #else
            SCITSCCH = 0x58 | (!(SciBuff & 0x01)?0x04:0);        // output compare, schedule clear output (start bit)                
    #endif
            SciBuff >>= 1;              // shift internal buffer
        }
        else if (SciCnt == 1)           // stop bit reached
        {
    #ifndef SCIINV
            SCITSCCH = 0x58 | 0x04;     // output compare, schedule set output (stop bit)            
    #else
            SCITSCCH = 0x58;     // output compare, schedule set output (stop bit)            
    #endif
            SCITCH = SciTmr = (SciTmr + BAUDTICK) & TMRMODULO;
            SciCnt--;                   // decrement counter
        }
        else
        {
            SCITSCCH = 0x00;        // port control, set output & disable further interrupts
            SCI0InterruptTx_CB();
        }
#else /* ifdef SCITXDPINISTIMERPIN */
        if (SciCnt > 1)
        {
    #ifndef SCIINV
            TXDPIN = SciBuff & 0x01;    // copy to TXD pin
    #else
            TXDPIN = ~(SciBuff & 0x01);    // copy to TXD pin
    #endif
            SciBuff >>= 1;              // shift internal buffer
            SCITCH = SciTmr = (SciTmr + BAUDTICK) & TMRMODULO;
            SciCnt--;                   // decrement counter
        }
        else if (SciCnt == 1)   // stop bit reached
        {
            TXDPINSET();        //stop bit
            SCITCH = SciTmr = (SciTmr + BAUDTICK) & TMRMODULO;
            SciCnt--;                   // decrement counter
        }
        else
        {
    #ifdef SCISINGLEWIRE
            TXDPINDDR = DDRIN;     // TXD pin input
    #endif
            SCITSC_IE = 0;          // disable further interrupts
            SCI0InterruptTx_CB();
        }
#endif /* ifdef SCITXDPINISTIMERPIN */

    }
    else /* if (SciStat == SCITX) */
    {

#ifdef SCIRXDPINISTIMERPIN
        if (SciCnt == 0)        // start bit falling edge captured
        {
            SCITSCCH = 0x50;    // timer int to be scheduled (keep int enabled)
            SCITCH = SciTmr = ((SciTmr = SCITCH) + BAUDTICK/2) & TMRMODULO;
            SciCnt++;           // first int will be useless (in the middle of start bit)
        }
        else
#endif /* ifdef SCIRXDPINISTIMERPIN */
        {
    #ifndef SCIINV
                if (SciPort & RXDPINMASK)
    #else
                if (!(SciPort & RXDPINMASK))
    #endif
                SciBuff = (SciBuff>>1) | 0x80;
            else
                SciBuff = (SciBuff>>1) & 0x7f;
            SCITCH = SciTmr = (SciTmr + BAUDTICK) & TMRMODULO;
            SciCnt++;

            if (SciCnt > 9)     // 9 bits because first is in start bit (*not used*)
            {
                SCDR = SciBuff;         // copy down the received buffer
                SCI0RxEnable();         // restore RX
                SCI0InterruptRx_CB();       // make RX interrupt!
           }
        }
    }  /* if (SciStat == SCITX) */

    SCITSC_CHF = 0;	        // clearing timer flag
}
