#! /usr/bin/python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Matheus Dal Mago <matheusdalmago10@hotmail.com>
#
# Distributed under terms of the MIT license.

"""

"""

serialPort = "COM1"
#serialPort = "/dev/tty.usbserial"
APN = "zap.vivo.com.br"
host = "simon-gpsnetcms.rhcloud.com"
API_KEY = "a4b245ca16c183c2ad2286efb342d09a"
monitorid = 10
IP = ""
baudRate = 19200
ser = None
connected = 0
lost_connection_cntr = 0


class TimeExpired(Exception):
    pass

def test_con():

    for i in range(3):
        ser.write("AT\r\n".encode("utf-8"))
        print("AT")

        ans = b""
        t = time.time()
        while ans.count(b"\r\n") == 0:
            if ser.in_waiting > 0:
                ans += ser.readline()
            if time.time() - t > 15:
                if i == 2:
                    return False
                else:
                    break

        print(ans)
        if ans.count(b"OK") != -1:
            return True

    return False

# Initial setup
def basicConfig():
    AT_commands = [
                "AT&K=0\r\n",                            # disable flow control
                "ATS12=2\r\n",                           # prompt time = 2 x 20ms
                "AT#GPRS=0\r\n",                         # deactivate PDP context
                "AT+CREG?\r\n",                          # read network report
                "AT+CGDCONT=1,\"IP\",\"" + APN + "\"\r\n",  # define PDP context
                "AT#GPRS?\r\n",
                "AT#SCFG=1,1,300,100,100,50\r\n",        # socket configuration
                #  conn ID, PDP ctx, pkt size, max timeout (s), conn. timeout (x100ms), Tx timeout (x100ms)
                "AT#SCFGEXT2=1,0,1,0,0\r\n",             # other socket config
                ]

    # Write commands in Serial and waits for the modem answer
    for command in AT_commands:
        ser.write(command.encode("utf-8"))
        print("\n" + command, end="")
        time.sleep(0.2)

        while ser.in_waiting > 0:
            ans = ser.readline()

            if ans != b"\r\n":
                print(ans)

# Get the IP address of the hostname
def getHostIP(hostname):
    while True:
        ser.write(("AT#QDNS=\"" + hostname + "\"\r\n").encode("utf-8"))
        print("\nAT#QDNS=\"" + hostname + "\"")
        time.sleep(2)

        ans = waitForLines(4)

        print(str(ans))
        if str(ans).find("NOT SOLVED") == -1:
            global IP
            IP = str(ans)[17+len(hostname) : -16] # get just the IP address

        time.sleep(2)

# Activate network operator context and open socket
def openSocket():
    while True:
        # activate context
        ser.write(("AT#SGACT?\r\n").encode("utf-8")) # read context status
        print("\nAT#SGACT?")
        time.sleep(1)

        ans = waitForLines(4)

        if str(ans).find("#SGACT: 1,1") != -1:
            break
        elif str(ans).find("#SGACT: 1,0") != -1:
            ser.write("AT#SGACT=1,1\r\n".encode("utf-8"))
            print("\nAT#SGACT=1,1")
            time.sleep(3)

            waitForLines(4)

        time.sleep(2)

    # connect
    time.sleep(2)

    i = 0
    while True:
        # open socket
        ser.write(("AT#SD=1,0,80," + host + ",0,0,0\r\n").encode("utf-8")) # open tcp socket
        print("\nAT#SD=1,0,80," + host + ",0,0,0")
        time.sleep(3)

        ans = waitForLines(2)

        if str(ans).find("CONNECT") != -1:
            global connected
            connected = 1
            break

        time.sleep(5)
        i+=1
        if i == 5:
            ser.write("AT#SGACT=1,1\r\n".encode("utf-8"))
            print("\nAT#SGACT=1,1")
            time.sleep(3)
            waitForLines(4)

        elif i == 7:
            raise Exception("Couldn't connect to tcp socket")

def closeSocket():
    ser.write("AT#SH=1\r\n".encode("utf-8"))  # close tcp socket
    print("\nAT#SH=1")
    time.sleep(0.2)
    waitForLines(2)
    global connected
    connected = 0

def sendTime():
    t = time.localtime()
    data = str(t.tm_mday) + "," + str(t.tm_hour) + "," + str(t.tm_min) + "," + str(t.tm_sec) + "," + str(lost_connection_cntr)

    ser.write(("GET /monitor/set.json?monitorid=" + str(monitorid) +
               "&data=" + data +
               "&apikey=" + API_KEY +
               " HTTP/1.1\r\nHost: " + host +
               "\r\n\r\n"
               ).encode("utf-8"))

    print(
        "\nGET /monitor/set.json?monitorid=" + str(monitorid) +
               "&data=" + data +
               "&apikey=" + API_KEY +
               " HTTP/1.1\r\nHost: " + host
          )

    time.sleep(3)
    ans = waitForLines(8)

    ser.write("+++".encode("utf-8"))
    print("\n+++")
    time.sleep(0.2)

    if ans.find(b"NO CARRIER") == -1: # not found
        waitForLines(2)

# Wait for the modem to answer l lines
def waitForLines(l):
    ans = b""
    t = time.time()
    while ser.in_waiting > 0 or ans.count(b"\r\n") < l:
        if ser.in_waiting > 0:
            ans += ser.readline()
        if time.time() - t > 25:
            raise TimeExpired("More than 25 secs without answer")
    print(ans)
    return ans


if __name__ == "__main__":
    import time
    try:
        import serial
    except ImportError:
        print("You must install pyserial, please run:")
        print("sudo pip3 install pyserial")
        exit()

    while True:
        ser = serial.Serial(serialPort, baudRate, timeout=3)
        ser.reset_input_buffer()
        ser.reset_output_buffer()
        try:
            basicConfig()
        except:
            ser.close()
            continue

        while True:
            try:
                ser.reset_input_buffer()
                ser.reset_output_buffer()
                openSocket()
                sendTime()
                closeSocket()
            except TimeExpired:
                if test_con() == False:
                    global lost_connection_cntr
                    lost_connection_cntr += 1
                    print("Lost Connection")

                # if connected == 1:
                #     closeSocket()
                # break

            except Exception:
                print ("Got Exception")
                if connected == 1:
                    closeSocket()
                break

            while time.localtime().tm_sec > 5:
                time.sleep(1)

        ser.close()
